/*
* ----------------------------------------------------------------------------
* "THE BEER-WARE LICENSE" (Revision 42):
* David Wischnjak wrote this file in the hope it will be useful.
* As long as you retain this notice you can do whatever you want with this stuff.
* If we meet some day, and you think this stuff is worth it, you can buy me a beer in return.
* ----------------------------------------------------------------------------
*/

/*
* Prog2Morse - Relay
* Class for controlling the LC-Tech Relay shield for ESP-01(S).
* This relay is not controlled by a digital pin, but instead it has it's own serial chip which listens on 9600 baud for a specific byte code.
* (Chinese documentation really sucks)
*/

#ifndef RELAY_H
#define RELAY_H

#include <Arduino.h>

#include "Debug.h"

const byte RELAY_ON[] = {0xA0, 0x01, 0x01, 0xA2};
const byte RELAY_OFF[] = {0xA0, 0x01, 0x00, 0xA1};

class Relay{
  private:
    bool relayState = false;
    
  public:
    void setState(bool relayState);
    bool getState();
};

#endif
