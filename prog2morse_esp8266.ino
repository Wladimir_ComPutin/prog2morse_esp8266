/*
* ----------------------------------------------------------------------------
* "THE BEER-WARE LICENSE" (Revision 42):
* David Wischnjak wrote this file in the hope it will be useful.
* As long as you retain this notice you can do whatever you want with this stuff.
* If we meet some day, and you think this stuff is worth it, you can buy me a beer in return.
* ----------------------------------------------------------------------------
*/

/*
* Prog2Morse
* Introduction to some Io(u)T (Internet of (useless) Things) for students @ Prog2 Lecture / LUH
*
* Quick'n'dirty ESP8266-code for controlling the LC-Tech Relay for ESP-01 via http.
* This stuff is based on my smartphone garage door R/C, but heavily simplified for teaching purposes.
* Don't use this for production systems as I removed anything security/encryption related.
*/

/*
* Prog2Morse - Main
* 
* Contains the main functionality.
* Entrypoint is setup()
*/

//OTA Update
#include <ESP8266HTTPUpdateServer.h>
#include <ESP8266WebServer.h>

//WiFi AccessPoint
#include <ESP8266WiFi.h>

#include "Debug.h"
#include "PersistentMemory.h"
#include "Relay.h"

const char FW_VERSION[] = "2.3";

String WIFISSID = "MorseBox-00"; //Default, change via COMMAND_SET_WIFI_SSID
String WIFIPASS = "prog2morse-000000";   //Default, change via COMMAND_SET_WIFI_PASS

//Constant strings
const char MESSAGE_BEGIN[] = "[BEGIN]";
const char MESSAGE_END[] = "[END]";

const char COMMAND_HELLO[] = "hellofriend";
const char COMMAND_RELAY_ON[] = "relay:on";
const char COMMAND_RELAY_OFF[] = "relay:off";
const char COMMAND_GET_RELAY_STATE[] = "getRelayState";
const char COMMAND_GET_STATUS[] = "getStatus";
const char COMMAND_SET_WIFI_SSID[] = "setSSID";
const char COMMAND_SET_WIFI_PASS[] = "setWIFIPass";
const char COMMAND_SAVE[] = "save";
const char COMMAND_PING[] = "ping";
const char COMMAND_REBOOT[] = "reboot";
const char COMMAND_RESET[] = "reset";
const char COMMAND_UPDATE[] = "update";

//TCP Server on Port 80 (HTTP)
WiFiServer server(80);

enum ProcessMessageResponse {ACK, ERR, DATA};
struct ProcessMessageStruct{
  ProcessMessageResponse responseCode;
  String responseData;
};

//OTA Update
ESP8266WebServer httpUpdateServer(8266);
ESP8266HTTPUpdateServer httpUpdater;
bool update_mode = false;
const char UPDATE_PATH[] = "/update";

Relay relay;
PersistentMemory mem;

void setupWiFi() {
  printDebug("Starting WiFi-AP: " + WIFISSID + ":" + WIFIPASS);
  WiFi.mode(WIFI_AP);
  WiFi.softAP(WIFISSID.c_str(), WIFIPASS.c_str());
}

//Well... this is kind of tricky to explain. Nah... nevermind!
ProcessMessageStruct writeSettings(String message, int min_length, int max_length, const char * command, MemMap addr, MemMap addr_set, String type) {
  printDebug(message);
  String setting = message.substring(message.indexOf(command) + strlen(command) + 1);
  printDebug("Writing setting: " + String(command) + " : " + setting);
  if (setting.length() >= min_length && setting.length() <= max_length) {
    mem.writeStringToEEPROM(addr, 64, setting);
    mem.writeBoolToEEPROM(addr_set, true);
    return {ACK,""};
  }
  return {ERR,"Parameter length not in [" + String(min_length) + "," + String(max_length) +"]"};
}

void loadSettings() {
  printDebug("Initialising PersistentMemory");
  mem.init();

  //clearEEPROM(MEM_FIRST, MEM_LAST);

  if (mem.readBoolFromEEPROM(MEM_WIFI_SSID_SET) == true) {
    WIFISSID = mem.readStringFromEEPROM(MEM_WIFI_SSID, 32);
  }
  printDebug("Loaded WIFISSID: " + WIFISSID);

  if (mem.readBoolFromEEPROM(MEM_WIFI_PASS_SET) == true) {
    WIFIPASS = mem.readStringFromEEPROM(MEM_WIFI_PASS, 63);
  }
  printDebug("Loaded WIFIPASS: " + WIFIPASS);
  
}

//If we run in Debug-mode, the relay is not attached so we can output debug info via Serial at 115200 baud
void initHardware() {
#if DEBUG == 1
  Serial.begin(115200);
  printDebug("Device running in Debug-Mode!");
#else
  Serial.begin(9600);
#endif
}

//Here's the entrypoint.
void setup() {
  initHardware();
  printDebug("Loading Firmware: " + String(FW_VERSION));
  loadSettings();
  setupWiFi();
  server.begin();
  printDebug("Bootsequence finished!\n");
}

//Actually just an empty HTTP 200 OK response.
//Yes, I know this could be a constant char[] but I love to waste resources!
String prepareACK() {
  String response =
    String("HTTP/1.1 200 OK\r\n") +
    "Connection: close\r\n" +  // the connection will be closed after completion of the response
    "\r\n";
  return response;
}

//An HTTP 400 Bad Request error + reason, so the dump user get to know where he fucked up.
String prepareError(String reason) {
  String response =
    String("HTTP/1.1 400 Bad Request\r\n") +
      "Connection: close\r\n" +  // the connection will be closed after completion of the response
      reason +
      "\r\n";
  return response;
}

//If we need to return some data in the HTTP response
String prepareData(String data) {
    String response =
    String("HTTP/1.1 200 OK\r\n") +
      "Connection: close\r\n" +  // the connection will be closed after completion of the response
      data +
      "\r\n";
  return response;
}

//Wrap the content in HTML, this makes debugging via webbrowser easier.
String prepareHTML(String data){
    String response =
      String("Content-Type: text/html\r\n") +
      "\r\n" +
      "<!DOCTYPE HTML>" +
      "<html>" +
      data +
      "</html>";
  return response;
}

String wrap(String data){
  return MESSAGE_BEGIN + data + MESSAGE_END;
}

String unwrap(String message) {
  String out;
  int startIndex = message.indexOf(MESSAGE_BEGIN);
  int endIndex = message.indexOf(MESSAGE_END, startIndex);

  if ((startIndex != -1) && (endIndex != -1) && (endIndex - startIndex <= 200)) {
    out = message.substring(startIndex + strlen(MESSAGE_BEGIN), endIndex);
  }
  out.trim();
  return out;
}

//Interprets message and returns its response to the caller.
//DATA = ACK + response String
ProcessMessageStruct processMessage(String message) {
  if (message == COMMAND_HELLO) {
    return {DATA, String("<head><meta charset=\"utf-8\"/><title>Surprise!</title></head><body>") +
    "<br>" +
"░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░<br>" + 
"░░░░░░░▄▄▀▀▀▀▀▀▀▀▀▀▄▄█▄░░░░▄░░░░█░░░░░░░<br>" +
"░░░░░░█▀░░░░░░░░░░░░░▀▀█▄░░░▀░░░░░░░░░▄░<br>" +
"░░░░▄▀░░░░░░░░░░░░░░░░░▀██░░░▄▀▀▀▄▄░░▀░░<br>" +
"░░▄█▀▄█▀▀▀▀▄░░░░░░▄▀▀█▄░▀█▄░░█▄░░░▀█░░░░<br>" +
"░▄█░▄▀░░▄▄▄░█░░░▄▀▄█▄░▀█░░█▄░░▀█░░░░█░░░<br>" +
"▄█░░█░░░▀▀▀░█░░▄█░▀▀▀░░█░░░█▄░░█░░░░█░░░<br>" +
"██░░░▀▄░░░▄█▀░░░▀▄▄▄▄▄█▀░░░▀█░░█▄░░░█░░░<br>" +
"██░░░░░▀▀▀░░░░░░░░░░░░░░░░░░█░▄█░░░░█░░░<br>" +
"██░░░░░░░░░░░░░░░░░░░░░█░░░░██▀░░░░█▄░░░<br>" +
"██░░░░░░░░░░░░░░░░░░░░░█░░░░█░░░░░░░▀▀█▄<br>" +
"██░░░░░░░░░░░░░░░░░░░░█░░░░░█░░░░░░░▄▄██<br>" +
"░██░░░░░░░░░░░░░░░░░░▄▀░░░░░█░░░░░░░▀▀█▄<br>" +
"░▀█░░░░░░█░░░░░░░░░▄█▀░░░░░░█░░░░░░░▄▄██<br>" +
"░▄██▄░░░░░▀▀▀▄▄▄▄▀▀░░░░░░░░░█░░░░░░░▀▀█▄<br>" +
"░░▀▀▀▀░░░░░░░░░░░░░░░░░░░░░░█▄▄▄▄▄▄▄▄▄██<br>" +
"░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░<br>" +
"</body>"};
  }

  if (message == COMMAND_RELAY_ON) {
    relay.setState(true);
    return {ACK, ""};
  }

  if (message == COMMAND_RELAY_OFF) {
    relay.setState(false);
    return {ACK, ""};
  }

  if (message == COMMAND_GET_RELAY_STATE){
    String data = "RELAY:" + String(relay.getState());
    return {DATA, data};
  }

  if (message == COMMAND_GET_STATUS){
    String data = "updatemode:" + String(update_mode) + "\n" +
                  "FW-Version: " + FW_VERSION;
    return {DATA, data};
  }

  if (message == COMMAND_PING) {
    printDebug("got Ping!");
    return {ACK, ""};
  }

  if (message == COMMAND_REBOOT) {
    ESP.restart();
    return {ACK, ""};
  }

  //Set new SSID
  if (message.startsWith(COMMAND_SET_WIFI_SSID)) {
    return writeSettings(message, 3, 32, COMMAND_SET_WIFI_SSID, MEM_WIFI_SSID, MEM_WIFI_SSID_SET, "string");
  }

  //Set new wifi pass
  if (message.startsWith(COMMAND_SET_WIFI_PASS)) {
    return writeSettings(message, 8, 63, COMMAND_SET_WIFI_PASS, MEM_WIFI_PASS, MEM_WIFI_PASS_SET, "string");
  }

  //Save settings. Reboot to apply them.
  if (message == COMMAND_SAVE) {
    mem.commit();
    return {ACK, ""};
  }

  //Don't mix up with COMMAND_REBOOT, this one will erase all settings and set them to default.
  if (message == COMMAND_RESET){
    mem.clearEEPROM(MEM_FIRST, MEM_LAST);
    return {ACK, ""};
  }

  //start OTA update server and return it's URL
  if (message == COMMAND_UPDATE){
    if(!update_mode) {
      printDebug("Enabling OTA Update");
      httpUpdater.setup(&httpUpdateServer, UPDATE_PATH);
      httpUpdateServer.begin();
      update_mode = true;
    }
    return {DATA, String("http://192.168.4.1:8266") + UPDATE_PATH}; //IP static for now...
  }

  //Got an unknown command... The user did something wrong, maybe I should return something more insulting...
  printDebug("Received unknown command:" + message);
  return {ERR, "Unknown Command"};
}

String receive_Data(WiFiClient &client) {
  while (client.connected()) {
    if (client.available()) {
      String httpMessage = client.readString();
      printDebug("\nReceived:\n" + httpMessage + "\n");
      String body = unwrap(httpMessage);
      printDebug("Message: " + body);
      return body;
    }
  }
  return "";
}

//send HTTP response to client and disconnect.
void send_Data(WiFiClient &client, String data) {
  printDebug("\nSending:\n" + data + "\n");
  client.println(data);
  delay(1);
  client.stop();
  printDebug("[Client disonnected]");
}

//HTTP server stuff
void doHttpServerStuff() {
  WiFiClient client = server.available();
  // wait for a client (web browser) to connect
  if (client) {
    printDebug("[Client connected]");
    client.setTimeout(100);

    String message = receive_Data(client);
    ProcessMessageStruct ret = processMessage(message);
    switch (ret.responseCode) {
      case ACK:
        send_Data(client, prepareACK());
      break;
      case DATA:
        send_Data(client, prepareData(prepareHTML(wrap(ret.responseData)))); 
      break;
      case ERR:
        send_Data(client, prepareError(prepareHTML(wrap(ret.responseData))));
      break;
    }
    client.stop(); //disconnect client (TCP, not WiFi)
  }
}


void loop() {
  doHttpServerStuff();

  //OTA Update
  if(update_mode){
    httpUpdateServer.handleClient();
  }
}
