# Prog2Morse ESP8266 firmware #
**Introduction to some Io*u*T (Internet of *useless* Things) for students @ Prog2 lecture / LUH**

Quick'n'dirty ESP8266-code for controlling the LC-Tech Relay for ESP-01 via http.
This stuff is based on my smartphone garage door R/C, but heavily simplified for teaching purposes.
Don't use this for production systems as I removed anything security/encryption related.

## Usage ##

If you just wanna know how to control this thing, head over to the [Wiki](https://bitbucket.org/Wladimir_ComPutin/prog2morse_esp8266/wiki/Home)

## Compile yourself (if you really want to) ##

* Install Arduino IDE
* In File > Preferences, add this Board Manager URL
```http://arduino.esp8266.com/stable/package_esp8266com_index.json```
* Go to Tools > Board > Boards Manager and install ESP8266 Board.
* ```git clone https://Wladimir_ComPutin@bitbucket.org/Wladimir_ComPutin/prog2morse_esp8266.git```
* Open prog2morse_esp8266.ino in Arduino IDE
* In Tools, set the following settings:
```
Board:           Generic ESP8266 Module
Flash Mode:      DIO
Flash Frequency: 40 MHz
CPU Frequency:   80 MHz
Flash Size (ESP-01S): 1M (64K SPIFFS)
Debug Port:      Disabled
Debug Level:     None
Reset Method:    ck
```
* Compile via STRG + R
* Go to Arduino build directory. (in Windows %user%\AppData\Local\Temp\arduino_build_*)
* Flash .bin via Webrowser / OTA

You'd better be sure not to break the OTA update functionality, otherwise you will be stuck with USB-flashing, which is shitty if you don't have a ESP8266 USB Serial Adapter.