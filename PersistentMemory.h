/*
* ----------------------------------------------------------------------------
* "THE BEER-WARE LICENSE" (Revision 42):
* David Wischnjak wrote this file in the hope it will be useful.
* As long as you retain this notice you can do whatever you want with this stuff.
* If we meet some day, and you think this stuff is worth it, you can buy me a beer in return.
* ----------------------------------------------------------------------------
*/

/*
* Prog2Morse - PersistentMemory
* We need persistent memory to remeber settings (like WiFi SSID and passphrase) after reboots.
* This class contains helper/wrapper functions. They simplify the usage of the EEPROM library, which is just some sort of bytewise access to flash memory.
* Don't forget to call .commit() after finishing your memory writing operations, otherwise any changes will be lost after reboot.
*/

#ifndef PMEM_H
#define PMEM_H

#include <Arduino.h>

#include <EEPROM.h>

#include "Debug.h"

//Persistent memory adresses. Otherwise we don't know at which address which setting is saved.
enum MemMap {
  MEM_FIRST = 0,
  MEM_WIFI_SSID = MEM_FIRST + 0,
  MEM_WIFI_PASS = MEM_WIFI_SSID + 33,
  MEM_WIFI_SSID_SET = MEM_WIFI_PASS + 64,
  MEM_WIFI_PASS_SET = MEM_WIFI_SSID_SET + 1,
  MEM_LAST = MEM_WIFI_PASS_SET + 1,
};
 
class PersistentMemory {
  private:
    const size_t BYTES = 512; //Size of EEPROM reserved memory. More than enough.
    
  public:

    void init();

    void clearEEPROM(enum MemMap start_index, enum MemMap end_index);
    void commit();
    
    String readStringFromEEPROM(enum MemMap start_index, uint16_t maxLength);
    void writeStringToEEPROM(enum MemMap start_index, uint16_t maxLength, String input);
    
    bool readBoolFromEEPROM(enum MemMap start_index);
    void writeBoolToEEPROM(enum MemMap start_index, bool input);
    
    int readIntFromEEPROM(enum MemMap start_index);
    void writeIntToEEPROM(enum MemMap start_index, int input);
    
};
 
#endif
