/*
* ----------------------------------------------------------------------------
* "THE BEER-WARE LICENSE" (Revision 42):
* David Wischnjak wrote this file in the hope it will be useful.
* As long as you retain this notice you can do whatever you want with this stuff.
* If we meet some day, and you think this stuff is worth it, you can buy me a beer in return.
* ----------------------------------------------------------------------------
*/

/*
* Prog2Morse - Relay
* (implementation)
*/

#include "Relay.h"

void Relay::setState(bool state){
  if(state == true){
    Serial.write(RELAY_ON, sizeof(RELAY_ON));
    printDebug("Relay up");
  } else {
    Serial.write(RELAY_OFF, sizeof(RELAY_OFF));
    printDebug("Relay down");
  }
  relayState = state;
}

bool Relay::getState(){
  return relayState;
}

