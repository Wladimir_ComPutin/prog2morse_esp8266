/*
* ----------------------------------------------------------------------------
* "THE BEER-WARE LICENSE" (Revision 42):
* David Wischnjak wrote this file in the hope it will be useful.
* As long as you retain this notice you can do whatever you want with this stuff.
* If we meet some day, and you think this stuff is worth it, you can buy me a beer in return.
* ----------------------------------------------------------------------------
*/

/*
* Prog2Morse - Debug
* 
* Debugging macro.
* If DEBUG is set to 1, printDebug() calls in this project get translated to Serial.println(). Otherwise they will be ignored by the compiler.
* (At least I hope while(false) will be ignored by the compiler, otherwise the compiler is dump)
*/

#ifndef DEBUG_H
#define DEBUG_H
  #include <Arduino.h>

  #define DEBUG 0 //Change to 1 for debug output
  
  #if DEBUG == 1
    #define printDebug(x) Serial.println(x)
  #else
    #define printDebug(x) while(false)
  #endif
  
#endif
